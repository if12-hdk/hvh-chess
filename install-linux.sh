
# Install script for Linux

if [ "$(id -u)" != "0" ]
then
   echo "Script should be executed as root."
else
    apt install nodejs npm
fi

cd src/server/
npm install websocket
cd ../../

rm -r src/server/tool/shared
mkdir src/server/tool/shared
cp -r src/shared src/server/tool

rm -r src/client/asset/js/shared
mkdir src/client/asset/js/shared
cp -r src/shared src/client/asset/js

echo "Script finished."
