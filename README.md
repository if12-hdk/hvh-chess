# Chess

## Why does this exist?
This is a simple multiplayer chess game based on NodeJS. It is being developed as a school project.

## Can I try it?
Sure, here's the [GitLab-Pages](https://if12-hdk.gitlab.io/hvh-chess).

To install, just clone or download the latest release and execute the install script according to your operating system.

### Linux
Write `./install-linux.sh` in the terminal.
### MacOS
Write `./install-mac.sh` in the terminal.
### Windows
Right-click **install-win-ps1** and execute with PowerShell.

Then open **/media/disconnected/Data/Git/Lab/hvh-chess/src/client/index.html** with a web-browser for an offline-version.

Run **runserver-linux-mac.sh** or **runserver-win.ps1** to start the server.
Afterwards type *127.0.0.1:8081* in the address-bar of your browser. You can change the IP and port in **src/shared/config.js**.

There you go!

## But I want to know more...
Me: [Max 'Myzh' Zimmermann](https://gitlab.com/Myzh)

School: [Hermann-von-Helmholtz Gymnasium Potsdam](https://helmholtzschule.de/)