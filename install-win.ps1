
# Install script for Windows

Write-Host "Script should be executed as root."

cinst nodejs -y
Set-Location -Path "src\server"
."C:\Program Files\nodejs\npm.cmd" install websocket
Set-Location -Path "..\..\"

Remove-Item "src\server\tool\shared" -Recurse
New-Item -Path "src\server\tool\shared" -ItemType directory
Copy-Item "src\shared\*" -Destination "src\server\tool\shared" -Recurse

Remove-Item "src\client\asset\js\shared" -Recurse
New-Item -Path "src\client\asset\js\shared" -ItemType directory
Copy-Item "src\shared\*" -Destination "src\client\asset\js\shared" -Recurse

Write-Host "Script finished."
