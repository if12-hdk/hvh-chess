
# Install script for MacOS

if [ "$(id -u)" != "0" ]
then
   echo "Script should be executed as root."
   exit 1
else
    brew install nodejs

    cd src/server/
    npm install websocket
    cd ../../

    rm -r src/server/tool/shared
    mkdir src/server/tool/shared
    cp -r src/shared src/server/tool

    rm -r src/client/asset/js/shared
    mkdir src/client/asset/js/shared
    cp -r src/shared src/client/asset/js
fi

echo "Script finished."
