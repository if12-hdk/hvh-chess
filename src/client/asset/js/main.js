// strict mode
"use strict";


// GLOBAL FUNCTIONS

function documentReady(func) {
    // execute function when document is ready

    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
        func();
    } else {
        document.addEventListener('DOMContentLoaded', func);
    }
}

function copy(object, origin, params) {
    // copys an object

    let copy = {};

    if (origin !== undefined) {
        copy = new origin(params[0], params[1]);
    }
    
    let key;
  
    for (key in object) {
        copy[key] = object[key];
    }

    return copy;
  }


// CLASS DECLARATIONS


// visualizing classes

class Menu {
    // visualizing class for the startup menu 

    constructor(parent) {

        parent.menu = this;

        parent.sound = new SoundOutput();
        parent.sound.load(config.private.soundEffects);

        this.visual = document.createElement("ul");
        this.visual.className = "menu";
        this.visual.setAttribute("data-uitheme", config.private.uiTheme);

        this.visual.hide = function() {
            this.style.display = "none";
        }

        this.visual.show = function() {
            this.style.display = "block";
        }

        var visual = this.visual;

        var local = document.createElement("li");
        local.className = "menuElement";
        local.innerText = "Local Game";
        local.addEventListener("click", function() {
            new GameModeLocal(parent);
            visual.hide();
        })
        this.visual.appendChild(local);
		
		var ai = document.createElement("li");
        ai.className = "menuElement";
        ai.innerText = "AI Game";
        ai.addEventListener("click", function() {
            new GameModeAi(parent);
            visual.hide();
        })
        this.visual.appendChild(ai);

        var remote = document.createElement("li");
        remote.className = "menuElement";
        remote.innerText = "Remote Game";
        remote.addEventListener("click", function() {
            new GameModeRemote(parent, parent);
            visual.hide();
        })
        this.visual.appendChild(remote);

        parent.appendChild(this.visual);
    }

}


class Board {
    // visualizing class for the game

    constructor(gamemode, parent) {

        this.gamemode = gamemode;
        this.game = gamemode.game;

        this.sound = parent.sound;
        this.menu = parent.menu;

        this.visual = document.createElement("div");
        this.visual.className = "gameBoard";
        this.visual.setAttribute("data-boardtheme", config.private.boardTheme);

        this.tiles = [];

        // create the DOM structure
        for (let i = 0; i <= 7; i++) {
            this.tiles[i] = [];

            var row = document.createElement("div");
            row.className = "gameBoardRow";

            for (let j = 0; j <= 7; j++) {
                this.tiles[i][j] = document.createElement("div");
                this.tiles[i][j].className = "gameBoardTile";

                this.tiles[i][j].setAttribute("data-x", i);
                this.tiles[i][j].setAttribute("data-y", j);

                // generate checkered pattern
                if ((i + j) % 2 == 0) {
                    this.tiles[i][j].className += " tileBlack";
                } else {
                    this.tiles[i][j].className += " tileWhite";
                }

                // initialize elements for game interaction
                this.tiles[i][j].overlay = document.createElement("div");
                this.tiles[i][j].overlay.className = "tileOverlay";
                this.tiles[i][j].overlay.board = this;
                this.tiles[i][j].overlay.reset = function () {
                    let selectors = document.querySelectorAll("[data-selector='move'], [data-selector='capture'], [data-selector='special']");

                    for (let k = 0; k < selectors.length; k++) {
                        selectors[k].setAttribute("data-selector", false);
                    }
                };
                this.tiles[i][j].overlay.func = this.tiles[i][j].overlay.reset;
                this.tiles[i][j].overlay.setFunction = function (func, self = this.board.tiles[i][j].overlay) {
                    self.removeEventListener("click", self.func);

                    self.func = func;
                    self.addEventListener("click", self.func);
                };

                this.tiles[i][j].overlay.setFunction(this.tiles[i][j].overlay.func);

                this.tiles[i][j].appendChild(this.tiles[i][j].overlay);

                row.appendChild(this.tiles[i][j]);
            }

            this.visual.appendChild(row);

        }

        this.indicatorBlack = document.createElement("div");
        this.indicatorBlack.className = "gameIndicator indicatorBlack";
        this.indicatorBlack.setAttribute("data-boardtheme", config.private.boardTheme);

            this.activeBlack = document.createElement("span");
            this.activeBlack.className = "indicatorActive indicatorWhite";
            this.activeBlack.setAttribute("data-boardtheme", config.private.uiTheme);

            this.visual.appendChild(this.activeBlack);

            var nameBlack = document.createElement("span");
            nameBlack.className = "indicatorName indicatorWhite";
            nameBlack.setAttribute("data-boardtheme", config.private.uiTheme);
            nameBlack.innerText = this.game.teamBlack.player.name;

            this.visual.appendChild(nameBlack);
        
        this.visual.appendChild(this.indicatorBlack);
        
        this.indicatorWhite = document.createElement("div");
        this.indicatorWhite.className = "gameIndicator indicatorWhite";
        this.indicatorWhite.setAttribute("data-boardtheme", config.private.boardTheme);

            this.activeWhite = document.createElement("span");
            this.activeWhite.className = "indicatorActive indicatorBlack";
            this.activeWhite.setAttribute("data-boardtheme", config.private.uiTheme);

            this.visual.appendChild(this.activeWhite);

            var nameWhite = document.createElement("span");
            nameWhite.className = "indicatorName indicatorBlack";
            nameWhite.setAttribute("data-boardtheme", config.private.uiTheme);
            nameWhite.innerText = this.game.teamWhite.player.name;

            this.visual.appendChild(nameWhite);
        
        this.visual.appendChild(this.indicatorWhite);

        parent.appendChild(this.visual);

        this.ui = document.createElement("div");
        this.ui.className = "gameUI";
        this.ui.setAttribute("data-uitheme", config.private.uiTheme);

            var resign = document.createElement("span");
            resign.className = "uiButton";
            resign.innerText = "Resign";
            resign.board = this;
            resign.addEventListener("click", function(){
                // resigns accordingly

                if (this.board.gamemode instanceof GameModeLocal) {
                    this.board.gamemode.game.resign(this.board.gamemode.game.active.colour);
                }
                else {
                    this.board.gamemode.game.resign(this.board.gamemode.game.teamWhite.colour);
                }
            })

            this.ui.appendChild(resign);

            if (!(this.gamemode instanceof GameModeAi)) {

                var propose = document.createElement("span");
                propose.className = "uiButton";
                propose.innerText = "Propose Draw";
                propose.board = this;
                propose.addEventListener("click", function(){
                    // proposes draw

                    if (this.board.gamemode instanceof GameModeLocal) {
                        this.board.gamemode.game.proposeDraw(this.board.gamemode.game.active.colour);
                    }
                    else if (this.board.gamemode instanceof GameModeRemote) {
                        this.board.gamemode.game.proposeDraw(this.board.gamemode.remote.team);
                    }
                });

                this.ui.appendChild(propose);
            }

            var seperator = document.createElement("span");
            seperator.className = "uiSeperator";
            this.ui.appendChild(seperator);

            var save = document.createElement("span");
            save.className = "uiButton";
            save.innerText = "Save";
            save.board = this;
            save.addEventListener("click", function(){
                // downloads the savegame as json

                var data = "data:text/json;charset=utf-8," + encodeURIComponent(this.board.gamemode.save());
                var hidden = document.createElement("a");
                document.body.appendChild(hidden);

                hidden.setAttribute("href", data);
                hidden.setAttribute("download", "savegame.json");
                hidden.click();

                document.body.removeChild(hidden);
            })

            this.ui.appendChild(save);

            if (!(this.gamemode instanceof GameModeRemote)) {
                var load = document.createElement("input");
                load.name = "loadGame";
                load.type = "file";
                load.className = "none";
                load.board = this;
                load.addEventListener("change", function(){
                    // loads a savegame from JSON

                    var board = this.board;

                    var reader = new FileReader();
                    reader.addEventListener("load", function(){
                        board.gamemode.load.call(board.gamemode, this.result);
                    })

                    reader.readAsText(this.files[0]);  
                });

                this.ui.appendChild(load);

                var label = document.createElement("label");
                label.htmlFor = "loadGame";
                label.className = "uiFile";
                label.innerText = "Load";
                label.addEventListener("click", function(){
                    document.getElementsByName(this.htmlFor)[0].click();
                });


                this.ui.appendChild(label);
            }

        parent.appendChild(this.ui);

    }

    update() {
        // updates game information gathered by the game class

        const info = this.game.state(); // gather current game data
        this.wipe(); // wipes the board to rewrite the game status

        switch (this.game.active.colour) {
            case "black":
                this.activeBlack.setAttribute("data-active", "true");
                this.activeWhite.setAttribute("data-active", "false");
                break;
            case "white":
                this.activeBlack.setAttribute("data-active", "false");
                this.activeWhite.setAttribute("data-active", "true");
                break;
        }

        for (let i = 0; i < info.length; i++) {
            if (info[i].alive) {
                var overlay = this.tiles[info[i].position[1]][info[i].position[0]].overlay;

                overlay.setAttribute("data-type", info[i].type);
                overlay.setAttribute("data-team", info[i].team);

                if (info[i].player == "local" && info[i].team == this.game.active.colour) {
                    overlay.event = overlay.setFunction(function () {
                        this.reset();

                        let j = 0;
                        while (j < info[i].moves.length) {
                            const move = info[i].moves[j];
                            var selector = this.board.tiles[move[1]][move[0]].overlay;

                            switch (move[2]) {
                                case 0:
                                    selector.setAttribute("data-selector", "move");
                                    break;
                                case 1:
                                    selector.setAttribute("data-selector", "capture");
                                    break;
                                case 2:
                                    selector.setAttribute("data-selector", "special");
                                    break;
                            }

                            overlay.event = selector.setFunction(function () {
                                this.board.execute.call(this.board, info[i], move);
                                this.reset();
                            });
                            j++;
                        }
                    });
                }
            }
			else {
				var figure = document.createElement("span");
				figure.className = "gameIndicator indicatorFigure";
				figure.setAttribute("data-type", info[i].type);
				
				switch (info[i].team) {
					case "black":
						this.indicatorBlack.appendChild(figure);
						break;
					case "white":
						this.indicatorWhite.appendChild(figure);
						break;
						
				}
			}
        }

        if (this.game.winner != null) {
            switch (this.game.winner) {
                case "black":
                    this.alert("Black wins!");
                    this.sound.play("checkmate");
                    break;
                case "white":
                    this.alert("White wins!");
                    this.sound.play("checkmate");
                    break;
                case "stalemate":
                    this.alert("Stalemate!");
                    this.sound.play("stalemate");
                    break;
            }

            var back = document.createElement("span");
            back.className = "uiButton";
            back.innerText = "Back to Menu";
            back.board = this;
            back.addEventListener("click", function(){
                this.board.menu.visual.show();
                this.board.delete();
            })

            this.ui.appendChild(back);
        }

    }

    alert(message, options, func) {
        // alert game event info

        let self = this;

        var alert = document.createElement("div");
        alert.className = "gameAlert";
        alert.innerText = message;

        if (options !== undefined) {
            for (let i = 0; i < options.length; i++) {
                let option = document.createElement("div");
                option.className = "gameAlert uiButton";
                option.innerText = options[i];
                option.option = i;
                option.addEventListener("click", function(){
                    func(self, option.option);
                    alert.parentElement.removeChild(alert);
                });
                alert.appendChild(option);
            }
        }
    
        this.visual.appendChild(alert);
    }

    wipe() {
        // wipes all entries on the board

        for (let i = 0; i <= 7; i++) {
            for (let j = 0; j <= 7; j++) {
                var overlay = this.tiles[i][j].overlay;
                overlay.className = "tileOverlay";
                overlay.setAttribute("data-type", "");
                overlay.setAttribute("data-team", "");
                overlay.setFunction(overlay.reset);
            }
        }
		
		this.indicatorBlack.innerHTML = "";
		this.indicatorWhite.innerHTML = "";

    }

    execute(figure, move) {
        // handles user input and forwards accordingly

        if (move[3] !== undefined && move[3]) {
            this.sound.play("promotion");
            switch (figure.type) {
                
                case "pawn":
                    this.alert("Choose a promotion", ["Queen", "Rook", "Bishop", "Knight"], function(self, option){
                        self.game.move.call(self.game, figure, move, undefined, ["queen", "rook", "bishop", "knight"][option]);
                    });
                    break;
                
                case "king":
                    this.game.move.call(this.game, figure, [move[0], move[1], move[2]], undefined, move[3]);
                    break;
            }
        }
        else {
            this.game.move.call(this.game, figure, move, undefined);

            if (move[2]) {
                this.sound.play("capture");
            }
            else {
                this.sound.play("move");
            }
        }
    }

    delete() {
        this.visual.parentNode.removeChild(this.visual);
        this.ui.parentNode.removeChild(this.ui);
        delete this.visual;
    }

}


class Matchmaking {
    // visualizing class for the matchmaking interface

    constructor(parent, server) {

        this.server = server;
        this.menu = parent.menu;

        this.id;
        this.name;
        this.list = [];

        // create DOM structure
        this.visual = document.createElement("div");
        this.visual.className = "matchmaking";

        this.visual.hide = function() {
            this.style.display = "none";
        }

        var enter = document.createElement("input");
        enter.className = "uiButton";
        enter.type = "text";
        enter.value = "Enter name";
        enter.matchmaking = this;
        enter.onchange = function() {
            this.matchmaking.server.sendCode("matchmaking", {
                command: "connect",
                parameters: [
                    this.value
                ]
            });
            this.parentNode.removeChild(this);
        }

        this.visual.appendChild(enter);

        var header = document.createElement("div");
        header.className = "matchmakingHeader";

        this.nameElm = document.createElement("span");
        this.nameElm.className = "headerName";
        header.appendChild(this.nameElm);

        this.idElm = document.createElement("span");
        this.idElm.className = "headerId";
        header.appendChild(this.idElm);

        this.visual.appendChild(header);

        this.listElm = document.createElement("ul");
        this.listElm.className = "matchmakingList";
        this.visual.appendChild(this.listElm);

        var back = document.createElement("ul");
        back.className = "uiButton";
        back.innerText = "Back to Menu";
        back.matchmaking = this;
        back.addEventListener("click", function(){
            this.matchmaking.menu.visual.show();
            this.matchmaking.visual.hide();
        })
        this.visual.appendChild(back);

        parent.appendChild(this.visual);

    }

    getName(id) {
        // returns name of a peer from an ID

        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i].id == id) {
                return this.list[i].name;
            }
        }
    }

    init(id, name) {
        // initializes basic info

        this.id = id;
        this.name = name;

        this.idElm.innerText = id;
        this.nameElm.innerText = name;
    }

    update(data) {
        // updates window information

        this.list = data;
        let server = this.server;

        this.wipe(); // wipe current info

        for (let i = 0; i < data.length; i++) {
            // create DOM structure
            var element = document.createElement("li");
            element.className = "listElement";

            var name = document.createElement("span");
            name.className = "listElementCell";
            name.innerText = data[i].name;
            element.appendChild(name);

            var id = document.createElement("span");
            id.className = "listElementCell";
            id.innerText = data[i].id;
            element.appendChild(id);

            if (data[i].id != this.id) {
                var challenge = document.createElement("span");
                challenge.className = "listElementCell";
                challenge.innerText = "challenge";
                challenge.addEventListener("click", function(){
                    server.sendCode("matchmaking", {
                        command: "challenge",
                        parameters: [
                            data[i].id
                        ]
                    });
                });
                element.appendChild(challenge);
            }

            this.listElm.appendChild(element);
        }
    }

    alert(data) {
        // alert challenge info
        
        let server = this.server;

        var alert = document.createElement("div");
        alert.className = "matchmakingAlert";
        alert.innerText = this.getName(data) + " challenged you to a match.";

        var accept = document.createElement("div");
        accept.className = "matchmakingAlert uiButton alertButtonAccept";
        accept.innerText = "Accept";
        accept.addEventListener("click", function(){
            server.sendCode("matchmaking", {
                command: "accept",
                parameters: [
                    data
                ]
            });
        });
        alert.appendChild(accept);

        var decline = document.createElement("div");
        decline.className = "matchmakingAlert uiButton alertButtonDecline";
        decline.innerText = "Decline";
        alert.appendChild(decline);

        this.visual.appendChild(alert);
    }

    wipe() {
        // wipes all entries on the window

        var elements = this.listElm.getElementsByClassName("listElement");

        for (let i = 0; i < elements.length; i++) {
            this.listElm.removeChild(elements[i]);
        }
    }

}


// game mode classes

class GameMode {

    save() {
        // saves a gamestate

        return JSON.stringify(this.game.moves);
    }

    load(savegame) {
        // loads a savegame

        savegame = JSON.parse(savegame);
        for (let i = 0; i < savegame.length; i++) {
            if (savegame[i].player == "black") {
                this.game.move(this.game.field.figures[savegame[i].figure], savegame[i].move, this.player2, savegame[i].addition);
            }
            else if (savegame[i].player == "white") {
                this.game.move(this.game.field.figures[savegame[i].figure], savegame[i].move, this.player1, savegame[i].addition);
            }
        }
    }

}

    class GameModeLocal extends GameMode {
        // local multiplayer game mode

        constructor(parent) {

            super();

            this.player1 = new PlayerLocal(this, true);
            this.player2 = new PlayerLocal(this, false);

            this.player1.name = "Player 1";
            this.player2.name = "Player 2";

            this.game = new Game(this.player1, this.player2); // assigns the local game class
            this.board = new Board(this, parent); // assigns the visualizing board

            this.board.update(); // first update to display initial positions

        }

    }
	
	class GameModeAi extends GameMode {
        // local singleplayer game mode

        constructor(parent) {

            super();

            this.player1 = new PlayerLocal(this);
            this.player2 = new PlayerAi(this);

            this.player1.name = "You";
            this.player2.name = "Computer";

            this.game = new Game(this.player1, this.player2); // assigns the local game class
            this.board = new Board(this, parent); // assigns the visualizing board

            this.board.update(); // first update to display initial positions

        }

    }

    class GameModeRemote extends GameMode {
        // remote multiplayer game mode

        constructor(parent) {

            super();
            delete this.load;

            this.server = new WebSocket("ws://" + config.shared.ip + ":" + config.shared.port);
            this.server.gamemode = this;

            this.server.sendCode = function (type, content) {
                this.send(JSON.stringify({
                    type: type,
                    content: content
                }))
            }

            this.matchmaking = new Matchmaking(parent, this.server);

            this.server.onopen = function () {
                console.log("Established connection");
            };

            this.server.onerror = function (error) {
                throw ("Network error");
            };

            this.server.onmessage = function (message) {

                var parser = new FileReader();
                var gamemode = this.gamemode;
                parser.onload = function() {
                    const data = JSON.parse(parser.result);

                    switch (data.type) {
                        case "matchmaking":
                            // handles all matchmaking related messages

                            switch (data.content.command) {
                                case "connect":
                                    // connects client to matchmaking

                                    gamemode.matchmaking.init(data.content.value[0], data.content.value[1]);
                                    break;

                                case "update":
                                    // updates the matchmaking window

                                    gamemode.matchmaking.update(data.content.value[0]);
                                    break;

                                case "challenge":
                                    // matches up clients

                                    gamemode.matchmaking.alert(data.content.value[0]);
                                    break;

                                case "accept":
                                    // handles a successful match

                                    if (data.content.value[0] == "black") {
                                        gamemode.player1 = new PlayerRemote(gamemode);
                                        gamemode.player2 = new PlayerLocal(gamemode);

                                        gamemode.player1.name = data.content.value[1];
                                        gamemode.player2.name = "You";

                                        gamemode.local = gamemode.player2;
                                        gamemode.remote = gamemode.player1;
                                    }
                                    else if (data.content.value[0] == "white") {
                                        gamemode.player1 = new PlayerLocal(gamemode);
                                        gamemode.player2 = new PlayerRemote(gamemode);

                                        gamemode.player1.name = "You";
                                        gamemode.player2.name = data.content.value[1];

                                        gamemode.local = gamemode.player1;
                                        gamemode.remote = gamemode.player2;
                                    }

                                    gamemode.matchmaking.visual.hide();

                                    gamemode.game = new Game(gamemode.player1, gamemode.player2); // assigns the local game class
                                    gamemode.board = new Board(gamemode, parent); // assigns the visualizing board

                                    gamemode.board.update(); // first update to display initial positions

                                    break;
                            }
                            break;

                        case "game":
                            // handles all in-game related messages

                            if (data.content !== undefined) {
                                if (data.content.player == "white") {
                                    var player = gamemode.game.teamWhite;
                                } else if (data.content.player == "black") {
                                    var player = gamemode.game.teamBlack.player;
                                }

                                const figure = gamemode.game.field.figures[data.content.figure];

                                gamemode.game.move(figure, data.content.move, player, data.content.addition);
                                gamemode.board.update();
                            }
                            break;

                        default:
                            // handles special cases

                            gamemode.local.hook("game", data.type);
                    }
                }
                
                parser.readAsText(message.data);
            };

        }

    }


    // player classes

    class Player {
        // abstract class for all players

        constructor(type) {

            this.type = type;

        }

    }

    class PlayerLocal extends Player {
        // local player

        constructor(gamemode, lead) {

            super("local");

            if (lead === undefined) {
                this.lead = gamemode !== undefined;
                if (this.lead) {
                    this.gamemode = gamemode;
                }
            }
            else {
                this.lead = lead;
                this.gamemode = gamemode;
            }

        }

        hook(mode, content) {
            // react to game state change

            if (this.gamemode instanceof GameModeLocal) {
                if (mode == "game" && content == "propose") {
                    this.gamemode.board.alert("Accept Draw?", ["Accept", "Decline"], function(self, option){
                        self.game.active.offer = true;
                        self.game.active.opponent.handleDrawProposal([true, false][option]);
                    });
                }
            }
            else {
                if (mode == "game" && content == "propose") {
                    this.gamemode.board.alert("Accept Draw?", ["Accept", "Decline"], function(self, option){
                        self.gamemode.remote.team.offer = true;
                        self.gamemode.local.team.handleDrawProposal([true, false][option]);
                    });
                }
                else if (mode == "game" && content == "draw") {
                    this.gamemode.local.team.offer = true;
                    this.gamemode.remote.team.handleDrawProposal(true);
                }
            }

            this.gamemode.board.update();

            if (this.gamemode instanceof GameModeAi) {
                this.team.opponent.player.hook.call(this.team.opponent.player); // call AI to play
            }
        }

    }
	
	class PlayerAi extends Player {
        // AI player

        constructor(gamemode, lead) {

            super("ai");

            if (lead === undefined) {
                this.lead = gamemode !== undefined;
                if (this.lead) {
                    this.gamemode = gamemode;
                }
            }
            else {
                this.lead = lead;
                this.gamemode = gamemode;
            }

        }

        hook(mode, content) {
            // react to game state change
            
            if (mode == "game" && content == "propose") {
                this.gamemode.game.teamBlack.handleDrawProposal(false);
            }
            else {
                // AI code

                function save(state) {
                    var positions = [];

                    for (let k = 0; k < state.field.figures.length; k++) {
                        positions.push([state.field.figures[k].position, state.field.figures[k].alive, state.field.figures[k].moved]);
                    }

                    return {
                        positions: positions,
                        history: state.moves.slice()
                    };
                }

                function restore(state, save) {
                    for (let k = 0; k < state.field.figures.length; k++) {
                        state.field.figures[k].position = save.positions[k][0];
                        state.field.figures[k].alive = save.positions[k][1];
                        state.field.figures[k].moved = save.positions[k][2];
                    }

                    state.moves = save.history;
                }

                function valuate(state, depth) {

                    const origin = save(state);

                    function stateWorth(state) {

                        var worth = 0;

                        for (let i = 0; i < state.field.figures.length; i++) {
                            if (state.field.figures[i].alive) {
                                if (state.field.figures[i].team.colour == "black") {
                                    worth += state.field.figures[i].worth;
                                }
                                else if (state.field.figures[i].team.colour == "white") {
                                    worth -= state.field.figures[i].worth;
                                }
                            }
                        }

                        return worth;
                    }
                    
                    var moves = [];
                    
                    for (let i = 0; i < state.active.figures.length; i++) {
                        const figureMoves = state.active.figures[i].moves();

                        for (let j = 0; j < figureMoves.length; j++) {
                            restore(state, origin);
                            let newState = copy(state, Game, [state.teamWhite.player, state.teamBlack.player]);
                            newState.move(newState.field.figures[i], figureMoves[j], newState.active, undefined, true);

                            if (depth > 0 && (stateWorth(newState) != stateWorth(state)) || depth > ((10 + state.moves.length) / 5) - 1) {
                                const list = valuate(newState, depth - 1);
                                var worth = 0;

                                for (let k = 0; k < list.length; k++) {
                                    worth = (worth * k + list[k]) / (k + 1);
                                }

                                moves.push(worth);
                            }
                            else {
                                moves.push(stateWorth(newState));
                            }
                        }
                    }

                    return moves;
                }

                const origin = save(this.gamemode.game);
                console.log((10 + this.gamemode.game.moves.length) / 5);

                const values = valuate(this.gamemode.game, (10 + this.gamemode.game.moves.length) / 5);
                restore(this.gamemode.game, origin);

                var index = 0;

                for (let i = 0; i < values.length; i++) {
                    if (values[i] > values[index]) {
                        index = i;
                    }
                }

                if (values[index] == values[values.length - 1] && index != values.length) {
                    index = Math.floor(Math.random() * (values.length - 1));
                }

                var moves = [];

                for (let i = 0; i < this.gamemode.game.active.figures.length; i++) {
                    const figure = this.gamemode.game.active.figures[i];

                    for (let j = 0; j < figure.moves().length; j++) {
                        moves.push([figure, figure.moves()[j]]);
                    }
                }

                console.log(moves.length, index);

                try {
                    this.gamemode.game.move(moves[index][0], moves[index][1], this);
                }
                catch (error) {
                    if (error instanceof TypeError) {
                        this.gamemode.game.move(moves[0][0], moves[0][1], this);
                    }
                }
            }

            this.gamemode.board.update();
        }

    }

    class PlayerRemote extends Player {
        // remote player

        constructor(gamemode) {

            super("remote");

            this.lead = false;
            this.gamemode = gamemode;

        }

        hook(mode, content) {
            // react to game state change

            if (mode === undefined) {
                this.gamemode.server.sendCode("game", this.gamemode.game.moves[this.gamemode.game.moves.length - 1]); // normal moves
            }
            else {
                // special moves
                if (mode == "game") {
                    this.gamemode.server.sendCode(content);
                }
            }
        }

    }


// audio classes

class SoundOutput {

    constructor() {

        this.buffer = null;
        this.context = new window.AudioContext();

        this.files = [];
        this.ready = false;
        
    }
  
    load(files) {
        function loadFile(i) {
            if (i < files.length) {
                this.files[i] = {
                    name: files[i].name
                };

                var request = new XMLHttpRequest();
                request.open("GET", files[i].url, true);
                request.responseType = "arraybuffer";
            
                var self = this;
                request.onload = function() {
                    self.context.decodeAudioData(request.response, function(buffer) {
                        self.files[i].buffer = buffer;
                        loadFile.call(self, i + 1)
                    });
                }
                request.send();
            }
        }

        loadFile.call(this, 0);

        this.ready = true;
    }
  
    start() {
        this.source.start(0);
    }
  
    stop() {
        this.source.stop(0);
    }
  
    play(title) {
        if (this.ready) {
            for (let i = 0; i < this.files.length; i++) {
                if (this.files[i].name == title) {
                    this.source = this.context.createBufferSource();
                    this.source.buffer = this.files[i].buffer;
                    this.source.connect(this.context.destination);
                    this.start();
                    break;
                }
            }
        }
    }
  
  }

