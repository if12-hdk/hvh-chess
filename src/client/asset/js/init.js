// strict mode
"use strict";


function init() {
    // init game

    // cross-browser compatability
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    if (!navigator.getUserMedia) {
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
    }
    
    window.AudioContext = window.AudioContext || window.webkitAudioContext;

    // start building process
    var menu = new Menu(document.getElementById("wrapper"));

}

// init game when document is ready
documentReady(init);