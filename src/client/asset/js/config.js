// strict mode
"use strict";


var config = {
    
    private: {
        // only client settings

        client: true,

        uiTheme: "dark", // default board theme
        boardTheme: "simple", // default board theme

        soundEffects: [
            {
                name: "move",
                url: "asset/audio/themes/standard/move.ogg"
            },
            {
                name: "capture",
                url: "asset/audio/themes/standard/capture.ogg"
            },
            {
                name: "promotion",
                url: "asset/audio/themes/standard/promotion.ogg"
            },
            {
                name: "checkmate",
                url: "asset/audio/themes/standard/checkmate.ogg"
            },
            {
                name: "stalemate",
                url: "asset/audio/themes/standard/stalemate.ogg"
            }
        ]
    }
};