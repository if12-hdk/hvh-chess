// strict mode
"use strict";

var http = require("http");
var fs = require("fs");
var path = require("path");
var WebSocketServer = require("websocket").server;

var Game = require("./shared/game");


class SocketInterface {
    // server class

    constructor(ip, port, root) {

        console.log("Starting HTTP server...");

        var httpServer = http.createServer(
            function (request, response) {

                console.log("Recieved HTTP request");

                var pathname = root + request.url;
                if (request.url == "/") {
                    pathname = pathname + "index.html";
                }

                var extension = path.extname(pathname);

                var type = "text/plain";
                switch (extension) {
                    case ".html":
                        type = "text/html";
                        break;
                    case ".js":
                        type = "text/javascript";
                        break;
                    case ".css":
                        type = "text/css";
                        break;
                    case ".ico":
                        type = "image/ico";
                        break;
                    case ".svg":
                        type = "image/svg+xml";
                        break;
                }

                fs.readFile(pathname, function (error, content) {
                    if (error) {
                        if (error.code == "ENOENT") {
                            response.writeHead(400);
                        } else {
                            response.writeHead(500);
                        }
                        response.end();
                    } else {
                        response.writeHead(200, {
                            "Content-Type": type
                        });
                        response.end(content, "utf-8");
                    }
                });
            }
        ).listen(port, ip);

        console.log("Starting WebSocket server...");
        var socketServer = new WebSocketServer({
            httpServer: httpServer,
            ips: ["192.168.1.11", "192.168.1.110"]
        });

        console.log("Starting matchmaking...");
        var matchmaking = new SocketMatchmaking();

        socketServer.on("request", function(request) {
            var connection = request.accept(null, request.origin);
            connection.gamemode;

            connection.sendCode = function(type, content) {
                connection.sendBytes(Buffer(JSON.stringify({
                    type: type,
                    content: content
                })));
            }

            connection.on("message", function(message) {
                console.log("Socket message recieved from " + connection.remoteAddress);

                if (message.type == "utf8") {
                    // handle client messages

                    var data = JSON.parse(message.utf8Data);

                    switch (data.type) {
                        case "matchmaking":
                            // redirect to matchmaking system

                            switch (data.content.command) {
                                case "connect":
                                    // connects client to matchmaking

                                    matchmaking.addClient(connection, data.content.parameters);
                                    connection.sendCode("matchmaking", {
                                        command: "connect",
                                        value: [
                                            connection.id,
                                            connection.name
                                        ]
                                    });

                                    // update peers
                                    matchmaking.callAll(function(connection){
                                        connection.sendCode("matchmaking", {
                                            command: "update",
                                            value: [
                                                matchmaking.listClients()
                                            ]
                                        });
                                    });
                                    break;

                                case "update":
                                    // updates client information

                                    connection.sendCode("matchmaking", {
                                        command: "update",
                                        value: [
                                            matchmaking.listClients()
                                        ]
                                    });
                                    break;

                                case "challenge":
                                    // matches up clients

                                    connection.challenger = matchmaking.getClient(data.content.parameters[0]);

                                    if (connection.id != connection.challenger.id) {
                                        connection.challenger.sendCode("matchmaking", {
                                            command: "challenge",
                                            value: [
                                                connection.id
                                            ]
                                        });
                                    }
                                    break;

                                case "accept":
                                    // creates game from match

                                    connection.challenger = matchmaking.getClient(data.content.parameters[0]);

                                    if (connection.id == connection.challenger.challenger.id) {
                                        connection.gamemode = new GameModeRemote(socketServer, connection, connection.challenger); // create new game
                                        connection.challenger.gamemode = connection.gamemode; // same with opponent

                                        connection.challenger.sendCode("matchmaking", {
                                            command: "accept",
                                            value: [
                                                "black", // assigns player colour
                                                connection.name // sends opponent's name
                                            ]
                                        });
                                        connection.sendCode("matchmaking", {
                                            command: "accept",
                                            value: [
                                                "white", // assigns player colour
                                                connection.challenger.name // sends opponent's name
                                            ]
                                        });
                                    }
                                    break;
                            }
        
                            break;
                        case "game":
                            // if already in a match

                            if (connection.gamemode !== undefined) {
                                console.log("Game event");
                                connection.gamemode.hook(data.content, connection);
                            }
                            break;

                        default:
                            // special events

                            if (connection.gamemode !== undefined) {
                                console.log("Special game event");
                                connection.gamemode.hook(data.type, connection);
                            }
                            break;
                    } 
                }
            });

            connection.on("close", function() {
                // handle connection close

                matchmaking.clenup(); // remove possible matchmaking listing
                console.log("Socket connection closed");
            });
        });

    }

}


class SocketMatchmaking {
    // class to match clients

    constructor() {

        this.clients = [];
        this.nextId = 0;

    }

    addClient(connection, parameters) {
        // adds client to matchmaking

        connection.name = parameters[0];
        connection.id = this.nextId;
        this.nextId++;

        this.clients[this.clients.length] = connection;

        connection.sendCode("matchmaking", {
            type: "connect",
            value: [
                connection.id,
                connection.name
            ]
        });
        console.log(connection.remoteAddress + " (" + connection.id + ") entered matchmaking");
    }

    clenup() {
        // removes idle and timeout clients from matchmaking
        
        for (let i = 0; i < this.clients.length; i++) {
            if (this.clients[i].state == "closed") {
                console.log("Removing leftover connection from matchmaking...");
                this.clients.splice(i, 1);
            }
        }
    }

    removeClient(id) {
        // removes a specific client from matchmaking

        for (let i = 0; i <= id; i++) {
            if (this.clients[i].id = id) {
                console.log("Removing " + this.clients[i].remoteAddress + " (" + this.clients[i].id + ") from matchmaking...");
                this.clients.splice(i, 1);
                break;
            }
        }
    }

    getClient(id) {
        // returns the client object from an ID

        for (let i = 0; i < this.clients.length; i++) {
            if (this.clients[i].id == id) {
                return this.clients[i];
            }
        }
    }

    listClients() {
        // returns a list of all available clients

        var clients = [];

        for (let i = 0; i < this.clients.length; i++) {
            clients[i] = {
                id: this.clients[i].id,
                name: this.clients[i].name,
            };
        }

        return clients;
    }

    callAll(func) {
        // calls all clients

        for (let i = 0; i < this.clients.length; i++) {
            func(this.clients[i]);
        }
    }

}


class GameModeRemote {
    // remote multiplayer game mode

    constructor(socket, connection1, connection2) {

        this.socket = socket;

        this.player1 = new PlayerRemote(this, connection1);
        this.player2 = new PlayerRemote(this, connection2);

        this.game = new Game(this.player1, this.player2); // assigns the local game class

    }

    hook(data, connection) {
        // react to client input

        if (this.player1.connection.id == connection.id) {
            var player = this.game.teamWhite;
        } else if (this.player2.connection.id == connection.id) {
            var player = this.game.teamBlack;
        } else {
            return false;
        }

        switch (data) {

            case "propose":
                this.game.proposeDraw(player.opponent.colour);
                break;

            case "draw":
                player.opponent.handleDrawProposal(true);
                break;

            default:
                this.game.move(this.game.field.figures[data.figure], data.move, player, data.addition);
        }
    }

}


class PlayerRemote {
    // remote player

    constructor(gamemode, connection) {

        this.type = "remote";
        this.gamemode = gamemode;
        this.connection = connection;
        this.ip = connection.remoteAddress;

    }

    hook(mode, content) {
        // react to game state change

        if (mode === undefined) {
            if (this.connection !== undefined) {
                this.connection.sendCode("game", this.gamemode.game.moves[this.gamemode.game.moves.length - 1]); // normal moves
            }
        }
        else {
            // special moves
            if (mode == "game") {
                this.connection.sendCode(content);
            }
        }
    }

}


module.exports = SocketInterface;
