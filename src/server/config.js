
// strict mode
"use strict";


module.exports = {
    private: {
        // only server-side settings

        client: false,
        path: "./src/client" // don't touch
    },
    shared: require("./../shared/config")  
};