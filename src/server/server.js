
// strict mode
"use strict";


// load configs
var config = require("./config");
var Network = require("./tool/network");

// start server
var server = new Network(config.shared.ip, config.shared.port, config.private.path);

if (process.argv[2] == "-t") {
    // unit testing

    console.log("Testing ended.")
    process.exit();
}
