
// strict mode
"use strict";


class Game {

    constructor(player1, player2) {

        this.field = {
            // stores and manages the figures and positions

            game: this,
            figures: [],
    
            addFigure(figure) {
                // adds a figure
    
                this.figures[this.figures.length] = figure;
            },
    
            removeFigure(figure) {
                // removes a figure
    
                var i = this.figures.indexOf(figure);
                if (i != -1) {
                    this.figures.splice(i, 1);
                }
            },

            checkPosition(position) {
                // checks if a position is occupied

                return (0 <= position[0] && position[0] <= 7 && 0 <= position[1] && position[1] <= 7);
            },
            
            checkOccupation(position, addition) {
                // returns the figure occupying a given position if present
    
                if (this.checkPosition(position)) {
                    if (addition !== undefined && position[0] == addition.theoreticalPosition[0] && position[1] == addition.theoreticalPosition[1]) {
                        return addition;
                    }

                    for (let i = 0; i < this.figures.length; i++) {
                        if (this.figures[i].alive) {
                            if (position[0] == this.figures[i].position[0] && position[1] == this.figures[i].position[1]) {
                                if (addition === undefined || !(position[0] == addition.position[0] && position[1] == addition.position[1])) {
                                    return this.figures[i];
                                } 
                            }
                        }
                    }
                    
                    return false;
                }
                else {
                    throw("Invalid position format.");
                }
    
            }
    
        };

        // initializes the teams
        this.teamWhite = new GameTeam(this, player1, "white");
        this.teamBlack = new GameTeam(this, player2, "black");

        this.teamBlack.opponent = this.teamWhite;
        this.teamWhite.opponent = this.teamBlack;

        this.active = this.teamWhite; // lets the white team begin

        // setup initial positions
        this.field.addFigure(new GameFigureRook(this.field, this.teamBlack, [0, 0]));
        this.field.addFigure(new GameFigureKnight(this.field, this.teamBlack, [1, 0]));
        this.field.addFigure(new GameFigureBishop(this.field, this.teamBlack, [2, 0]));
        this.field.addFigure(new GameFigureQueen(this.field, this.teamBlack, [3, 0]));
        this.field.addFigure(new GameFigureKing(this.field, this.teamBlack, [4, 0]));
        this.field.addFigure(new GameFigureBishop(this.field, this.teamBlack, [5, 0]));
        this.field.addFigure(new GameFigureKnight(this.field, this.teamBlack, [6, 0]));
        this.field.addFigure(new GameFigureRook(this.field, this.teamBlack, [7, 0]));

        this.field.addFigure(new GameFigurePawn(this.field, this.teamBlack, [0, 1]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamBlack, [1, 1]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamBlack, [2, 1]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamBlack, [3, 1]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamBlack, [4, 1]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamBlack, [5, 1]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamBlack, [6, 1]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamBlack, [7, 1]));
        
        
        this.field.addFigure(new GameFigureRook(this.field, this.teamWhite, [0, 7]));
        this.field.addFigure(new GameFigureKnight(this.field, this.teamWhite, [1, 7]));
        this.field.addFigure(new GameFigureBishop(this.field, this.teamWhite, [2, 7]));
        this.field.addFigure(new GameFigureQueen(this.field, this.teamWhite, [3, 7]));
        this.field.addFigure(new GameFigureKing(this.field, this.teamWhite, [4, 7]));
        this.field.addFigure(new GameFigureBishop(this.field, this.teamWhite, [5, 7]));
        this.field.addFigure(new GameFigureKnight(this.field, this.teamWhite, [6, 7]));
        this.field.addFigure(new GameFigureRook(this.field, this.teamWhite, [7, 7]));

        this.field.addFigure(new GameFigurePawn(this.field, this.teamWhite, [0, 6]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamWhite, [1, 6]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamWhite, [2, 6]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamWhite, [3, 6]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamWhite, [4, 6]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamWhite, [5, 6]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamWhite, [6, 6]));
        this.field.addFigure(new GameFigurePawn(this.field, this.teamWhite, [7, 6]));

        this.moves = [];
        this.winner = null;

    }

    state() {
        // returns the current state of the game

        let state = [];

        for (let i = 0; i < this.field.figures.length; i++) {
            state[i] = this.field.figures[i].info();
        }

        return state;

    }

    move(figure, move, player, addition, testing) {
        // allows a player to make a move

        testing = testing | testing !== undefined;

        const fieldFigure = this.field.figures[figure.id];
        const lastMove = this.moves[this.moves.length - 1];

        if (lastMove === undefined || !(figure != lastMove.figure && move != lastMove.move) || (figure.team == this.active.colour || figure.team.colour == this.active.colour)) {
            if (player === undefined || player == this.active || player == this.active.player) {
                if (figure.id == fieldFigure.info().id && (figure.team == this.active.colour || figure.team.colour == this.active.colour)) {
                    for (let i = 0; i < fieldFigure.info().moves.length; i++) {
                        if (fieldFigure.info().moves[i][0] == move[0] && fieldFigure.info().moves[i][1] == move[1]) {
                            if (fieldFigure.move([move[0], move[1]], move[2], addition)) { // makes the move
                                if (fieldFigure.team.checkmate()) {
                                    this.winner = "stalemate"; // declares a stalemate
                                }
                                else {
                                    this.winner = this.active.colour;
                                }
                            }
                            this.moves[this.moves.length] = {
                                figure: figure.id,
                                move: move,
                                player: this.active.colour,
                                addition: addition
                            }; // push into moves array
                            this.switchActive(); // lets the opponent make their move
                            if (!testing) {
                                this.inform(); // inform players
                            }
                            break;
                        }
                    }
                }
            }
            else {
                console.log("Not your turn");
            }
        }
        else if (!testing) {
            console.log("Move successful");
        }
    }

    resign(team) {
        switch (team) {
            case "black":
                team = this.teamBlack;
                break;
            case "white":
                team = this.teamWhite;
                break;
        }

        team.resign();
        this.inform(); // inform players
    }

    proposeDraw(team) {
        switch (team) {
            case "black":
                team = this.teamBlack;
                break;
            case "white":
                team = this.teamWhite;
                break;
        }

        team.proposeDraw();
        this.inform(); // inform players
    }

    switchActive() {
        // switches the active player

        if (this.active == this.teamBlack) {
            this.active = this.teamWhite;
        }
        else {
            this.active = this.teamBlack;
        }
    }

    inform() {
        // informs the players of a give move

        this.teamWhite.player.hook();
        if (this.teamBlack.player.type == "remote") {
            this.teamBlack.player.hook();
        }
    }

}

class GameTeam {
    // class to store team information

    constructor(game, player, colour) {

        this.game = game;
        this.player = player;
        this.colour = colour;
        this.figures = [];

        this.offer = false;

        player.team = this;

    }

    checkmate() {
        // checks if player is in checkmate

        for (let i = 0; i < this.figures.length; i++) {
            const moves = this.figures[i].moves();

            let j = 0;
            while (j < moves.length) {
                if (!this.king.inCheck(this.figures[i], [moves[j][0], moves[j][1]])) {
                    return false;
                }
            }
        }

        return true;
    }

    resign() {
        this.game.winner = this.opponent.colour;
    }

    proposeDraw() {
        this.offer = true;
        this.player.hook("game", "propose");
    }

    handleDrawProposal(response) {
        if (response && this.opponent.offer) {
            if (!this.offer) {
                this.player.hook("game", "draw");
                this.opponent.player.hook("game", "draw");
            }
            this.game.winner = "stalemate"; // declares a stalemate
            this.game.inform(); // inform players
        }
    }

}

class GameFigure {
    // abstract class for all figures

    constructor(field, team, position) {
        if (field !== undefined) {
            this.field = field;
            this.id = this.field.figures.length;
            if (team.colour == "black" || team.colour == "white") {
                this.team = team;
            }
            this.team.figures[this.team.figures.length] = this;

            this.position = null;
            this.alive = true;

            this.move(position);
            this.moved = 0;
        }
    }

    move(position, capture, addition) {
        // moves the figure

        if (capture === undefined) {
            capture = false;
        }

        if (!this.field.checkOccupation(position) || capture) {
            if (capture) {
                if (this.field.checkOccupation(position).team != this.team && capture == 1) {
                    this.field.checkOccupation(position).kill();
                }
                else if (this.field.checkOccupation([position[0], this.position[1]]) && capture == 2) {
                    if (this.field.checkOccupation([position[0], this.position[1]]).team != this.team) {
                        this.field.checkOccupation([position[0], this.position[1]]).kill();
                    }
                }
                else if (capture != 2) {
                    throw("Can't capture own figures");
                }   
            }

            this.position = position;

            if (this.type == "pawn" && ((this.team.colour == "black" && position[1] == 7) || (this.team.colour == "white" && position[1] == 0))) {
                // handles promotion for pawns
                switch (addition) {
                    case "queen":
                        this.changeRole(new GameFigureQueen());
                        break;
                    case "rook":
                        this.changeRole(new GameFigureRook());
                        break;
                    case "bishop":
                        this.changeRole(new GameFigureBishop());
                        break;
                    case "knight":
                        this.changeRole(new GameFigureKnight());
                        break;
                }
            }
            else if (this.type == "king" && addition !== undefined) {
                // handles casting move for the king
                this.field.figures[addition[0]].move([addition[1][0], addition[1][1]], addition[1][2]);
            }

            this.moved++;

            if (this.moved !== undefined) {
                return this.team.opponent.checkmate();
            }

            return false;
        }
        else {
            throw("Position already occupied");
        }
    }

    changeRole(figure) {
        // changes the type of the figure

        this.type = figure.type;
        this.theoreticalMoves = figure.theoreticalMoves;
    }

    info() {
        // returns info

        return {
            id: this.id,
            type: this.type,
            team: this.team.colour,
            player: this.team.player.type,
            alive: this.alive,
            position: this.position,
            moves: this.moves()
        }
    }

    moves() {
        // returns every possible move

        if (this.alive) {
            var moves = this.theoreticalMoves();

            let i = 0;
            while (i < moves.length) {
                if (this.team.king.inCheck(this, [moves[i][0], moves[i][1]])) {
                    moves.splice(i, 1);
                    i--;
                }
                i++;
            }

            return moves;
        }
        else {
            return [];
        }
    }

    kill() {
        // kills the figure

        this.alive = false;
        this.position = null;
    }

}

    class GameFigurePawn extends GameFigure {

        constructor(field, team, position) {

            super(field, team, position);
            this.type = "pawn";

            this.worth = 1;

        }

        theoreticalMoves(addition) {
            var moves = [];

            // specify moving direction
            if (this.team.colour == "black") {
                var direction = 1;
            }
            else {
                var direction = -1;
            }

            for (let i = -1; i <= 1; i++) {
                var position = [this.position[0] + i, this.position[1] + direction];

                // if promotable
                position[3] = (position[1] == 7 && this.team.colour == "black") || (position[1] == 0 && this.team.colour == "white");

                if (this.field.checkPosition(position)) {
                    if (i != 0 && this.field.checkOccupation(position, addition)) {
                        if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                            // add capture moves
                            position[2] = 1;
                            moves[moves.length] = position;
                        }
                    }
                    else if (i == 0 && !this.field.checkOccupation(position, addition)) {
                        // add standard move
                        position[2] = 0;
                        moves[moves.length] = position;
                        
                        if (!this.moved) {
                            // add special first move
                            switch (this.team.colour) {
                                case "black":
                                    position = [position[0], position[1] + 1, 0];
                                    if (!this.field.checkOccupation(position, addition)) {
                                        moves[moves.length] = position;
                                    }
                                    break;
                                case "white":
                                    position = [position[0], position[1] - 1, 0];
                                    if (!this.field.checkOccupation(position, addition)) {
                                        moves[moves.length] = position;
                                    }
                                    break;
                            }
                        }
                    }
                    else if (this.moved > 0 && i != 0 && this.field.checkOccupation([position[0], this.position[1]]) && this.field.game.moves[this.field.game.moves.length - 1] !== undefined) {
                        if (this.field.checkOccupation([position[0], this.position[1]]).team.colour != this.team.colour && ((this.team.colour == "black" && this.position[1] == 4) || (this.team.colour == "white" && this.position[1] == 3)) && this.field.checkOccupation([position[0], this.position[1]]).moved == 1 && this.field.game.moves[this.field.game.moves.length - 1].figure == this.field.checkOccupation([position[0], this.position[1]]).id) {
                            // add en passant capture move
                            position[2] = 2;
                            moves[moves.length] = position;
                        }
                    }
                }
                
            }

            return moves;
        }

    }

    class GameFigureRook extends GameFigure {

        constructor(field, team, position) {

            super(field, team, position);
            this.type = "rook";

            this.worth = 6;

        }

        theoreticalMoves(addition) {
            var moves = [];

            var position = [this.position[0], this.position[1] - 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0], position[1] - 1];
            }

            var position = [this.position[0] - 1, this.position[1]];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] - 1, position[1]];
            }

            var position = [this.position[0], this.position[1] + 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0], position[1] + 1];
            }

            var position = [this.position[0] + 1, this.position[1]];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] + 1, position[1]];
            }

            return moves;
        }

    }

    class GameFigureKnight extends GameFigure {

        constructor(field, team, position) {

            super(field, team, position);
            this.type = "knight";

            this.worth = 4;

        }

        theoreticalMoves(addition) {
            var moves = [
                [this.position[0] - 2, this.position[1] + 1],
                [this.position[0] - 2, this.position[1] - 1],
                [this.position[0] - 1, this.position[1] + 2],
                [this.position[0] - 1, this.position[1] - 2],
                [this.position[0] + 2, this.position[1] + 1],
                [this.position[0] + 2, this.position[1] - 1],
                [this.position[0] + 1, this.position[1] + 2],
                [this.position[0] + 1, this.position[1] - 2]
            ];

            for(let i = 0; i < moves.length; i++) {
                var position = moves[i];

                if (this.field.checkPosition(position)) {
                    if (this.field.checkOccupation(position, addition)) {
                        if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                            moves[i].push(1);
                        }
                        else {
                            moves.splice(i, 1);
                            i--;
                        }
                    }
                    else {
                        moves[i].push(0);
                    }
                }
                else {
                    moves.splice(i, 1);
                    i--;
                }
            }

            return moves;
        }

    }

    class GameFigureBishop extends GameFigure {

        constructor(field, team, position) {

            super(field, team, position);
            this.type = "bishop";

            this.worth = 3;

        }

        theoreticalMoves(addition) {
            var moves = [];

            var position = [this.position[0] - 1, this.position[1] - 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] - 1, position[1] - 1];
            }

            var position = [this.position[0] - 1, this.position[1] + 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] - 1, position[1] + 1];
            }

            var position = [this.position[0] + 1, this.position[1] - 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] + 1, position[1] - 1];
            }

            var position = [this.position[0] + 1, this.position[1] + 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] + 1, position[1] + 1];
            }

            return moves;
        }

    }

    class GameFigureQueen extends GameFigure {

        constructor(field, team, position) {

            super(field, team, position);
            this.type = "queen";

            this.worth = 9;

        }

        theoreticalMoves(addition) {
            var moves = [];

            var position = [this.position[0], this.position[1] - 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0], position[1] - 1];
            }

            var position = [this.position[0] - 1, this.position[1]];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] - 1, position[1]];
            }

            var position = [this.position[0], this.position[1] + 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0], position[1] + 1];
            }

            var position = [this.position[0] + 1, this.position[1]];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] + 1, position[1]];
            }

            var position = [this.position[0] - 1, this.position[1] - 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] - 1, position[1] - 1];
            }

            var position = [this.position[0] - 1, this.position[1] + 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] - 1, position[1] + 1];
            }

            var position = [this.position[0] + 1, this.position[1] - 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] + 1, position[1] - 1];
            }

            var position = [this.position[0] + 1, this.position[1] + 1];
            while (this.field.checkPosition(position)) {
                if (this.field.checkOccupation(position, addition)) {
                    if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                        position[2] = 1;
                        moves[moves.length] = position;
                    }
                    break;
                }
                else {
                    position[2] = 0;
                    moves[moves.length] = position;
                }
                position = [position[0] + 1, position[1] + 1];
            }

            return moves;
        }

    }

    class GameFigureKing extends GameFigure {

        constructor(field, team, position) {

            super(field, team, position);
            this.type = "king";
            this.team.king = this;

            this.worth = 1000;

        }

        theoreticalMoves(addition) {
            var moves = [
                [this.position[0], this.position[1] - 1],
                [this.position[0] - 1, this.position[1]],
                [this.position[0], this.position[1] + 1],
                [this.position[0] + 1, this.position[1]],
                [this.position[0] - 1, this.position[1] - 1],
                [this.position[0] - 1, this.position[1] + 1],
                [this.position[0] + 1, this.position[1] - 1],
                [this.position[0] + 1, this.position[1] + 1]
            ];

            for(let i = 0; i < moves.length; i++) {
                var position = moves[i];

                if (this.field.checkPosition(position)) {
                    if (this.field.checkOccupation(position, addition)) {
                        if (this.field.checkOccupation(position, addition).team.colour != this.team.colour) {
                            moves[i].push(1)
                        }
                        else {
                            moves.splice(i, 1);
                            i--;
                        }
                    }
                    else {
                        moves[i].push(0);
                    }
                }
                else {
                    moves.splice(i, 1);
                    i--;
                }
            }

            // castling move
            if (this.moved == 0 && !this.inCheck(this, this.position)) {
                if (this.team.colour == "black") {
                    if (this.field.figures[0].moved == 0 && this.field.figures[0].alive && !this.field.checkOccupation([1, 0]) && !this.field.checkOccupation([2, 0]) && !this.field.checkOccupation([3, 0])) {
                        moves[moves.length] = [2, 0, 2, [0, [3, 0, 2]]];
                    }
                    if (this.field.figures[7].moved == 0 && this.field.figures[7].alive && !this.field.checkOccupation([5, 0]) && !this.field.checkOccupation([6, 0])) {
                        moves[moves.length] = [6, 0, 2, [7, [5, 0, 2]]];
                    }
                }
                else if (this.team.colour == "white") {
                    if (this.field.figures[23].moved == 0 && this.field.figures[23].alive && !this.field.checkOccupation([1, 7]) && !this.field.checkOccupation([2, 7]) && !this.field.checkOccupation([3, 7])) {
                        moves[moves.length] = [6, 7, 2, [23, [5, 7, 2]]];
                    }
                    if (this.field.figures[16].moved == 0 && this.field.figures[16].alive && !this.field.checkOccupation([5, 7]) && !this.field.checkOccupation([6, 7])) {
                        moves[moves.length] = [2, 7, 2, [16, [3, 7, 2]]];
                    }
                }
            }

            // special king - king checkmate situation
            for (let i = 0; i < moves.length; i++) {
                if (1 >= Math.abs(moves[i][0] - this.team.opponent.king.position[0]) && 1 >= Math.abs(moves[i][1] - this.team.opponent.king.position[1])) {
                    moves.splice(i, 1);
                    i--;
                }
            }

            return moves;
        }

        inCheck(figure, position) {
            // checks if king is in check

            figure.theoreticalPosition = position;

            if (this.position == null) {
                return true;
            }

            for (let i = 0; i < this.field.figures.length; i++) {
                if (this.field.figures[i].team.colour != this.team.colour && this.field.figures[i].type != "king" && this.field.figures[i].alive && this.field.checkPosition(position) && this.field.figures[i].id != this.field.checkOccupation(position).id) {

                    const moves = this.field.figures[i].theoreticalMoves(figure);

                    for (let j = 0; j < moves.length; j++) {
                        if (((figure.id == this.id && (moves[j][0] == position[0] && moves[j][1] == position[1])) || (figure.id != this.id && (moves[j][0] == this.position[0] && moves[j][1] == this.position[1]))) && moves[j][2] == 1) {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

    }

    
if (typeof config == "undefined") {
    // only for NodeJS
    
    module.exports = Game;
}
