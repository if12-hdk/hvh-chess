
// strict mode
"use strict";


let sharedConfig = {
    // shared config

    ip: "127.0.0.1", // enter your servers IP
    port: 8082, // enter the port
}


if (typeof config == "undefined") {
    // only for NodeJS

    module.exports = sharedConfig;
}
else {
    // for client

    config.shared = sharedConfig;
}
